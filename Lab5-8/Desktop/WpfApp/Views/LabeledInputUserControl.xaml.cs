﻿using System.Windows;
using System.Windows.Controls;

namespace WpfApp.Views
{
    /// <summary>
    /// Interaction logic for LabeledInputUserControl.xaml
    /// </summary>
    public partial class LabeledInputUserControl : UserControl
    {
        public LabeledInputUserControl()
        {
            InitializeComponent();
        }



        public string LabelContent
        {
            get { return (string)GetValue(LabelContentProperty); }
            set { SetValue(LabelContentProperty, value); }
        }

        // Using a DependencyProperty as the backing store for LabelContent.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LabelContentProperty =
            DependencyProperty.Register("LabelContent", typeof(string), 
                typeof(LabeledInputUserControl), new PropertyMetadata("", new PropertyChangedCallback(OnLabelContentChanged)));

        private static void OnLabelContentChanged(DependencyObject d,
         DependencyPropertyChangedEventArgs e)
        {
            LabeledInputUserControl UserControl = d as LabeledInputUserControl;
            UserControl.OnLabelContentChanged(e);
        }

        private void OnLabelContentChanged(DependencyPropertyChangedEventArgs e)
        {
            label.Content = e.NewValue.ToString();
        }


        public string TextBoxText
        {
            get { return (string)GetValue(TextBoxTextProperty); }
            set { SetValue(TextBoxTextProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TextBoxText.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TextBoxTextProperty =
            DependencyProperty.Register("TextBoxText", typeof(string),
                typeof(LabeledInputUserControl), new PropertyMetadata(""));


    }
}
