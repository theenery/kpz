﻿using System.Windows;
using WpfApp.ViewModels;

namespace WpfApp
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private MainWindowViewModel _viewModel;

        public App()
        {
            _viewModel = new MainWindowViewModel();
            var window = new MainWindow() { DataContext = _viewModel };
            window.Show();
        }
    }
}
