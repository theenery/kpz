﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WpfApp.Models;

namespace WpfApp.Services
{
    internal class DetailService
    {
        private HttpClient _client = new HttpClient();

        public async Task<IEnumerable<Detail>> GetDetails()
        {
            var response = await _client.GetAsync("https://localhost:8002/api/Detail");
            var readed = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<IEnumerable<Detail>>(readed);
            return result;
        }

        public async Task<Detail> CreateDetail(Detail detail)
        {
            var content = JsonConvert.SerializeObject(new { 
                DetailName = detail.DetailName,
                DetailPrice = detail.DetailPrice,
                DetailCount = detail.DetailCount 
            });
            var data = new StringContent(content, Encoding.UTF8, "application/json");
            var response = await _client.PostAsync($"https://localhost:8002/api/Detail", data);
            var readed = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<Detail>(readed);
            return result;
        }

        public async Task<bool> UpdateDetail(Detail detail)
        {
            var content = JsonConvert.SerializeObject(detail);
            var data = new StringContent(content, Encoding.UTF8, "application/json");
            var response = await _client.PutAsync($"https://localhost:8002/api/Detail/{detail.DetailId}", data);
            return response.IsSuccessStatusCode;
        }

        public async Task<bool> DeleteDetail(int id)
        {
            var response = await _client.DeleteAsync($"https://localhost:8002/api/Detail/{id}");
            return response.IsSuccessStatusCode;
        }
    }
}
