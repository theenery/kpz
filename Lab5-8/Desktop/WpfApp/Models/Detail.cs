﻿namespace WpfApp.Models
{
    internal class Detail
    {
        public int DetailId { get; set; }
        public string DetailName { get; set; }
        public decimal? DetailPrice { get; set; }
        public int DetailCount { get; set; }
    }
}
