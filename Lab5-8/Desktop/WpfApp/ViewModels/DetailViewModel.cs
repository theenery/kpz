﻿using WpfApp.Models;
using WpfApp.MVVM;

namespace WpfApp.ViewModels
{
    internal class DetailViewModel: ViewModelBase
    {
        public Detail Detail { get; private set; }

        public DetailViewModel(Detail detail)
        {
            Detail = detail;
        }

        public int DetailId
        {
            get { return Detail.DetailId; }
            set
            {
                Detail.DetailId = value;
                OnPropertyChanged("DetailId");
            }
        }

        public string DetailName
        {
            get { return Detail.DetailName; }
            set
            {
                Detail.DetailName = value;
                OnPropertyChanged("DetailName");
            }
        }

        public decimal? DetailPrice
        {
            get { return Detail.DetailPrice; }
            set
            {
                Detail.DetailPrice = value;
                OnPropertyChanged("DetailPrice");
            }
        }

        public int DetailCount
        {
            get { return Detail.DetailCount; }
            set
            {
                Detail.DetailCount = value;
                OnPropertyChanged("DetailCount");
            }
        }

        public override bool Equals(object obj)
        {
            var other = obj as DetailViewModel;
            if (other is null) {
                return false;
            }
            return DetailId == other.DetailId && DetailName == other.DetailName 
                && DetailPrice == other.DetailPrice && DetailCount == other.DetailCount;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
