﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using WpfApp.Models;
using WpfApp.MVVM;
using WpfApp.Services;

namespace WpfApp.ViewModels
{
    internal class MainWindowViewModel: ViewModelBase
    {
        public ObservableCollection<DetailViewModel> Details { get; set; }
        private DetailService _detailService = new DetailService();

        public MainWindowViewModel()
        {
            SaveCommand = new DelegateCommand(Save, CanSave);
            DeleteCommand = new DelegateCommand(Delete, CanDelete);
            Details = new ObservableCollection<DetailViewModel>();
            _detailService.GetDetails().ContinueWith(task =>
            {
                if (task.Result != null)
                {
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        Details.Clear();
                        foreach(Detail detail in task.Result)
                        {
                            Details.Add(new DetailViewModel(detail));
                        }
                    });
                }
            });
        }

        private DetailViewModel _currentDetail = new DetailViewModel(new Detail());
        public DetailViewModel CurrentDetail
        {
            get { return _currentDetail; }
            set
            {
                _currentDetail = value;
                OnPropertyChanged("CurrentDetail");
            }
        }

        private DetailViewModel _selectedDetail;
        public DetailViewModel SelectedDetail
        {
            get { return _selectedDetail; }
            set
            {
                if (value == null)
                {
                    CurrentDetail = new DetailViewModel(new Detail());
                }
                else
                {
                    CurrentDetail = new DetailViewModel(new Detail
                    {
                        DetailId = value.DetailId,
                        DetailName = value.DetailName,
                        DetailPrice = value.DetailPrice,
                        DetailCount = value.DetailCount
                    });
                }
                _selectedDetail = value;
                OnPropertyChanged("SelectedDetail");
            }
        }

        public ICommand SaveCommand { get; set; }
        public void Save()
        {
            if (CurrentDetail.DetailId == 0)
            {
                _detailService.CreateDetail(CurrentDetail.Detail).ContinueWith(task =>
                {
                    if (task.Result != null)
                    {
                        Application.Current.Dispatcher.Invoke(() => 
                        Details.Add(new DetailViewModel(task.Result)));
                    }
                    else
                    {
                        MessageBox.Show("Can`t create this detail");
                    }
                });
            }
            else
            {
                _detailService.UpdateDetail(CurrentDetail.Detail).ContinueWith(task =>
                {
                    if (task.Result)
                    {
                        var detail = Details.SingleOrDefault(d => d.DetailId == CurrentDetail.DetailId);
                        detail.DetailName = CurrentDetail.DetailName;
                        detail.DetailPrice = CurrentDetail.DetailPrice;
                        detail.DetailCount = CurrentDetail.DetailCount;
                    }
                    else
                    {
                        MessageBox.Show("Can`t update this detail");
                    }
                });
            }
        }

        public bool CanSave()
        {
            return !(string.IsNullOrEmpty(CurrentDetail.DetailName));
        }

        public ICommand DeleteCommand { get; set; }
        public void Delete()
        {
            _detailService.DeleteDetail(_currentDetail.DetailId).ContinueWith(task =>
            {
                if (task.Result)
                {
                    Application.Current.Dispatcher.Invoke(() => Details.Remove(CurrentDetail));
                }
                else
                {
                    MessageBox.Show("Can`t delete this detail");
                }
            });
        }

        public bool CanDelete()
        {
            return Details.FirstOrDefault(d => d.Equals(CurrentDetail)) != null;
        }
    }
}
