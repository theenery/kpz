import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from '../../../shared/components/header/header.component';
import { Service } from '../../../core/models/service';
import { ServiceService } from '../../../core/services/service.service';
import { NonePipe } from '../../../shared/pipes/none.pipe';
import { FormGroup, NonNullableFormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { PrettyInputDirective } from '../../../shared/directives/pretty-input.directive';
import { OutsideClickDirective } from '../../../shared/directives/outside-click.directive';
import { MatIconModule } from '@angular/material/icon';

@Component({
  selector: 'app-service-list',
  standalone: true,
  imports: [CommonModule, HeaderComponent, MatIconModule, NonePipe, OutsideClickDirective, PrettyInputDirective, ReactiveFormsModule],
  templateUrl: './service-list.component.html',
  styleUrls: ['./service-list.component.scss']
})
export class ServiceListComponent implements OnInit {
  currentServiceForm: FormGroup;
  services: Service[] = [];
  showModal = false;

  constructor(
    private serviceService: ServiceService,
    private formBuilder: NonNullableFormBuilder
  ) {
    this.currentServiceForm = formBuilder.group({
      serviceId: [0],
      serviceName: ['', [Validators.required]],
      servicePrice: [null, [Validators.required, Validators.pattern('\\d+(\\.\\d+)?')]],
      serviceGuarantee: ['']
    });
  }

  close() {
    this.showModal = false;
  }

  create() {
    this.serviceService
      .create(this.currentServiceForm.value)
      .subscribe(
        result => {
          this.services.push(result);
        },
        error => {
          alert('no');
        });
    this.close();
  }

  delete() {
    this.serviceService
      .delete(this.currentServiceForm.value.serviceId)
      .subscribe(
        result => {
          this.services = this.services.filter(s => s.serviceId != this.currentServiceForm.value.serviceId);
        },
        error => {
          alert('no');
        }
    );
    this.close();
  }

  edit(service: Service) {
    this.showModal = true;
    this.currentServiceForm.setValue(service);
  }

  ngOnInit() {
    this.serviceService.getAll().subscribe(result => {
      this.services = result;
    });
  }

  open() {
    this.currentServiceForm.reset();
    this.showModal = true;
  }

  save() {
    if (this.currentServiceForm.value.serviceId) {
      this.update();
    }
    else {
      this.create();
    }
  }

  update() {
    this.serviceService
      .update(this.currentServiceForm.value)
      .subscribe(
        result => {
          let service = this.services.find(s =>
            s.serviceId == this.currentServiceForm.value.serviceId) ?? {};
          Object.assign(service, this.currentServiceForm.value);
        },
        error => {
          alert('no');
        });
    this.close();
  }
}
