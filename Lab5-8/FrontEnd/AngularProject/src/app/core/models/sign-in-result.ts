export interface SignInResult {
  firstname: string,
  lastname: string,
  role: string,
  token: string
}
