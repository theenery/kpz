export interface CreateService {
  serviceName: string,
  servicePrice: number,
  serviceGuarantee: string
}
