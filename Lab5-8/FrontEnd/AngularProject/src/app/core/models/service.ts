export interface Service {
  serviceId: number,
  serviceName: string,
  servicePrice: number,
  serviceGuarantee: string
}
