import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { SignInModel } from '../models/sign-in';
import { JwtHelperService } from '@auth0/angular-jwt';
import { SignUpModel } from '../models/sign-up';
import { SignInResult } from '../models/sign-in-result';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  jwtHelper = new JwtHelperService();

  constructor(private http: HttpClient) { }

  isSignedIn() {
    return !this.jwtHelper.isTokenExpired(localStorage.getItem('jwt_token'));
  }

  signIn(credentials: SignInModel) {
    return this.http.post<SignInResult>('https://localhost:8002/api/Account', credentials)
      .pipe(
        map(result => {
          if (result && result.token) {
            localStorage.setItem('jwt_token', result.token);
            localStorage.setItem('firstname', result.firstname);
            localStorage.setItem('lastname', result.lastname);
          }
          return result;
        })
      );
  }

  signOut() {
    localStorage.removeItem('jwt_token');
    localStorage.removeItem('firstname');
    localStorage.removeItem('lastname');
  }

  signUp(credentials: SignUpModel) {
    return this.http.put('https://localhost:8002/api/Account', credentials);
  }
}
