import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CreateService } from '../models/create-service';
import { Service } from '../models/service';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  constructor(private http: HttpClient) { }

  create(service: CreateService) {
    return this.http.post<Service>('https://localhost:8002/api/service', service);
  }

  delete(id: number) {
    return this.http.delete('https://localhost:8002/api/service/' + id);
  }

  getAll() {
    return this.http.get<Service[]>('https://localhost:8002/api/service');
  }

  get(id: number) {
    return this.http.get('https://localhost:8002/api/service/' + id);
  }

  update(service: Service) {
    return this.http.put('https://localhost:8002/api/service/' + service.serviceId, service);
  }
}
