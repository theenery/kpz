import { Routes } from '@angular/router';
import { SignInComponent } from './features/components/sign-in/sign-in.component';
import { SignUpComponent } from './features/components/sign-up/sign-up.component';
import { ServiceListComponent } from './features/components/service-list/service-list.component';
import { authGuard } from './core/guards/auth.guard';

export const routes: Routes = [
  {
    path: 'services', loadComponent: () => 
      import('./features/components/service-list/service-list.component')
        .then(mod => mod.ServiceListComponent),
    canActivate: [authGuard]
  },
  { path: 'sign-in', component: SignInComponent },
  { path: 'sign-up', component: SignUpComponent },
  { path: '', redirectTo: 'sign-in', pathMatch: 'full' }
];
