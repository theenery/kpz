import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appPrettyInput]',
  standalone: true
})
export class PrettyInputDirective {

  constructor(private element: ElementRef) {
    this.element.nativeElement.style.background = 'rgba(0, 0, 0, 0.1)';
    this.element.nativeElement.style.border = 'none';
    this.element.nativeElement.style.borderRadius = '1rem';
    this.element.nativeElement.style.padding = '1rem';
    this.element.nativeElement.style.width = '100%';
    this.element.nativeElement.style.fontSize = 'inherit';
    this.element.nativeElement.style.fontWeight = 'inherit';
  }
}
