import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { AuthService } from '../../../core/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  standalone: true,
  imports: [CommonModule, MatButtonModule, MatIconModule, MatMenuModule],
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  providers: [AuthService]
})
export class HeaderComponent {
  @Input() header = '';
  username = localStorage.getItem('firstname') + ' ' + localStorage.getItem('lastname');

  constructor(
    private authService: AuthService,
    private router: Router) { }

  signOut() {
    this.authService.signOut();
    this.router.navigate(['']);
  }
}
