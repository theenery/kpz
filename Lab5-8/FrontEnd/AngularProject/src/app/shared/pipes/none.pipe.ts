import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'none',
  standalone: true
})
export class NonePipe implements PipeTransform {

  transform(value: unknown, ...args: unknown[]): unknown {
    if (value) {
      return value;
    }
    else {
      return 'none';
    }
  }

}
