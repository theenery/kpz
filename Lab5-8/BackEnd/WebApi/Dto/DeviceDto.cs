﻿namespace WebApi.Dto
{
    public class DeviceDto
    {
        public int DeviceId { get; set; }
        public int DeviceTypeId { get; set; }
        public string DeviceMaker { get; set; } = null!;
        public string? DeviceModel { get; set; }
    }
}
