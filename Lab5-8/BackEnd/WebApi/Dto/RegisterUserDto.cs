﻿namespace WebApi.Dto
{
    public class RegisterUserDto
    {
        public string Email { get; set; } = null!; 
        public string Firstname { get; set; } = null!;
        public string Lastname { get; set; } = null!;
        public string Password { get; set; } = null!;
    }
}
