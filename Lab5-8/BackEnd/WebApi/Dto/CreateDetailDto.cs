﻿namespace WebApi.Dto
{
    public class CreateDetailDto
    {
        public string DetailName { get; set; } = null!;
        public decimal? DetailPrice { get; set; }
        public int DetailCount { get; set; }
    }
}
