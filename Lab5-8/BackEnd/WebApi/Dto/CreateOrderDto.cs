﻿namespace WebApi.Dto
{
    public class CreateOrderDto
    {
        public int ClientId { get; set; }
        public int ServiceId { get; set; }
    }
}
