﻿namespace WebApi.Dto
{
    public class ServiceDto
    {
        public int ServiceId { get; set; }
        public string ServiceName { get; set; } = null!;
        public decimal ServicePrice { get; set; }
        public string? ServiceGuarantee { get; set; }
    }
}
