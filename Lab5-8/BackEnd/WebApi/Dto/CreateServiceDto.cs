﻿namespace WebApi.Dto
{
    public class CreateServiceDto
    {
        public string ServiceName { get; set; } = null!;
        public decimal ServicePrice { get; set; }
        public string? ServiceGuarantee { get; set; }
    }
}
