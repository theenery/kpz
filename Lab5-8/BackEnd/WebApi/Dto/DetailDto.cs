﻿namespace WebApi.Dto
{
    public class DetailDto
    {
        public int DetailId { get; set; }
        public string DetailName { get; set; } = null!;
        public decimal? DetailPrice { get; set; }
        public int DetailCount { get; set; }
    }
}
