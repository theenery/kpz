﻿namespace WebApi.Dto
{
    public class DeviceTypeDto
    {
        public int DeviceTypeId { get; set; }
        public string DeviceTypeName { get; set; } = null!;
    }
}
