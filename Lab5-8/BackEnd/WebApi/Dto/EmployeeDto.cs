﻿namespace WebApi.Dto
{
    public class EmployeeDto
    {
        public int UserId { get; set; }
        public string EmployeePass { get; set; } = null!;
        public int JobId { get; set; }
        public int DepartmentId { get; set; }
    }
}
