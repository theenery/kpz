﻿using System.Net;

namespace WebApi.Middleware
{
    public class ExceptionHandlingMiddleware
    {
        private readonly RequestDelegate _next;

        public ExceptionHandlingMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception e)
            {
                await HandleExceptionAsync(context, e);
            }
        }

        private async Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            ExceptionResponse response = exception switch
            {
                ApplicationException => new ExceptionResponse(
                    HttpStatusCode.BadRequest,
                    "Application exception occurred."),
                KeyNotFoundException => new ExceptionResponse(
                    HttpStatusCode.NotFound,
                    "The request key not found."),
                UnauthorizedAccessException => new ExceptionResponse(
                    HttpStatusCode.Unauthorized,
                    "Unauthorized."),
                _ => new ExceptionResponse(
                    HttpStatusCode.InternalServerError,
                    "Internal server error. Please retry later.")
            };

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)response.StatusCode;
            await context.Response.WriteAsJsonAsync(response);
        }

        public record ExceptionResponse(HttpStatusCode StatusCode, string Description);
    }
}
