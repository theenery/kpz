﻿using WebApi.Models;

namespace WebApi.Repository
{
    public interface IOrderRepository : IGenericRepository<Order>
    {

    }
}
