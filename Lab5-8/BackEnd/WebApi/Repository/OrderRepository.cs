﻿using WebApi.Data;
using WebApi.Models;

namespace WebApi.Repository
{
    public class DetailRepository : GenericRepository<Detail>, IDetailRepository
    {
        public DetailRepository(WebApiDbContext context) : base(context)
        {

        }
    }
}
