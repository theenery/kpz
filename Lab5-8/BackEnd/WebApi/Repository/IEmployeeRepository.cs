﻿using WebApi.Models;

namespace WebApi.Repository
{
    public interface IEmployeeRepository : IGenericRepository<Employee>
    {

    }
}
