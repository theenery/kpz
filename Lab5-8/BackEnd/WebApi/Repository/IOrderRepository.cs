﻿using WebApi.Models;

namespace WebApi.Repository
{
    public interface IDetailRepository : IGenericRepository<Detail>
    {

    }
}
