﻿using WebApi.Models;

namespace WebApi.Repository
{
    public interface IDeviceRepository : IGenericRepository<Device>
    {

    }
}
