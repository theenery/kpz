﻿using WebApi.Models;

namespace WebApi.Repository
{
    public interface IServiceRepository : IGenericRepository<Service>
    {

    }
}
