﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using WebApi.Data;

namespace WebApi.Repository
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        private readonly WebApiDbContext _context;
        private readonly DbSet<T> _dbSet;

        public GenericRepository(WebApiDbContext context)
        {
            _context = context;
            _dbSet = context.Set<T>();
        }

        public virtual IQueryable<T> GetAll()
        {
            return _dbSet.AsQueryable();
        }

        public virtual async Task<T?> GetByIdAsync(int id)
        {
            return await _dbSet.FindAsync(id);
        }

        public virtual async Task CreateAsync(T entity)
        {
            await _dbSet.AddAsync(entity);
            await _context.SaveChangesAsync();
        }

        public virtual async Task<bool> Exists(int id)
        {
            return await _dbSet.FindAsync(id) != null;
        }

        public virtual async Task UpdateAsync(int id, T entity)
        {
            var result = await _dbSet.FindAsync(id) ?? throw new Exception("Entity with id was not found");
            _context.Entry(result).CurrentValues.SetValues(entity);
            await _context.SaveChangesAsync();
        }

        public virtual async Task DeleteAsync(int id)
        {
            T? result = await _dbSet.FindAsync(id) ?? throw new Exception("Entity with id was not found");
            _dbSet.Remove(result);
            await _context.SaveChangesAsync();
        }
    }
}
