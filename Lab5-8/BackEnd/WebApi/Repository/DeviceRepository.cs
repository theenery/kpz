﻿using WebApi.Data;
using WebApi.Models;

namespace WebApi.Repository
{
    public class DeviceRepository : GenericRepository<Device>, IDeviceRepository
    {
        public DeviceRepository(WebApiDbContext context) : base(context)
        {

        }
    }
}
