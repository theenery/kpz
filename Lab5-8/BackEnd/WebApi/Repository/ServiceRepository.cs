﻿using WebApi.Data;
using WebApi.Models;

namespace WebApi.Repository
{
    public class ServiceRepository : GenericRepository<Service>, IServiceRepository
    {
        public ServiceRepository(WebApiDbContext context) : base(context)
        {

        }
    }
}
