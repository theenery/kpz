﻿namespace WebApi.Repository
{
    public interface IGenericRepository<T> where T : class
    {
        Task CreateAsync(T entity);
        Task DeleteAsync(int id);
        Task<bool> Exists(int id);
        IQueryable<T> GetAll();
        Task<T?> GetByIdAsync(int id);
        Task UpdateAsync(int id, T entity);
    }
}