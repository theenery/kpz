﻿using WebApi.Data;
using WebApi.Models;

namespace WebApi.Repository
{
    public class OrderRepository : GenericRepository<Order>, IOrderRepository
    {
        public OrderRepository(WebApiDbContext context) : base(context)
        {

        }
    }
}
