﻿using System;
using System.Collections.Generic;

namespace WebApi.Models
{
    public partial class Department
    {
        public Department()
        {
            Employees = new HashSet<Employee>();
        }

        public int DepartmentId { get; set; }
        public string DepartmentAdress { get; set; } = null!;
        public string DepartmentCity { get; set; } = null!;

        public virtual ICollection<Employee> Employees { get; set; }
    }
}
