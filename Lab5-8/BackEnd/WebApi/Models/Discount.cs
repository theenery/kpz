﻿using System;
using System.Collections.Generic;

namespace WebApi.Models
{
    public partial class Discount
    {
        public Discount() { 
            Clients = new HashSet<Client>();
            DeviceTypes = new HashSet<DeviceType>();
        }

        public int DiscountId { get; set; }
        public DateTime DiscountStarting { get; set; }
        public DateTime DiscountExpiring { get; set; }
        public double DiscountValue { get; set; }

        public virtual ICollection<Client> Clients { get; set; }
        public virtual ICollection<DeviceType> DeviceTypes { get; set; }
    }
}
