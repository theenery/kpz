﻿using System;
using System.Collections.Generic;

namespace WebApi.Models
{
    public partial class Employee
    {
        public Employee() { 
            Orders = new HashSet<Order>();
        }

        public int UserId { get; set; }
        public string EmployeePass { get; set; } = null!;
        public int JobId { get; set; }
        public int DepartmentId { get; set; }

        public virtual Department Department { get; set; } = null!;
        public virtual Job Job { get; set; } = null!;
        public virtual User User { get; set; } = null!;
        public virtual ICollection<Order> Orders { get; set; }
    }
}
