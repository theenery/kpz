﻿using System;
using System.Collections.Generic;

namespace WebApi.Models
{
    public partial class Client
    {
        public Client()
        {
            Discounts = new HashSet<Discount>();
            Orders = new HashSet<Order>();
            Rewievs = new HashSet<Rewiev>();
        }

        public int UserId { get; set; }
        public string? ClientCity { get; set; }

        public virtual User User { get; set; } = null!;
        public virtual ICollection<Discount> Discounts { get; set; }
        public virtual ICollection<Order> Orders { get; set; }
        public virtual ICollection<Rewiev> Rewievs { get; set; }
    }
}
