﻿using System;
using System.Collections.Generic;

namespace WebApi.Models
{
    public partial class Service
    {
        public Service()
        {
            Orders = new HashSet<Order>();
        }

        public int ServiceId { get; set; }
        public string ServiceName { get; set; } = null!;
        public decimal ServicePrice { get; set; }
        public string? ServiceGuarantee { get; set; }

        public virtual ICollection<Order> Orders { get; set; }
    }
}
