﻿using System;
using System.Collections.Generic;

namespace WebApi.Models
{
    public partial class Rewiev
    {
        public int RewievId { get; set; }
        public int ClientId { get; set; }
        public byte RewievRate { get; set; }
        public string? RewievText { get; set; }
        public DateTime RewievDate { get; set; }

        public virtual Client Client { get; set; } = null!;
    }
}
