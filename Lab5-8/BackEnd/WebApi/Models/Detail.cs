﻿using System;
using System.Collections.Generic;

namespace WebApi.Models
{
    public partial class Detail
    {
        public Detail()
        {
            Devices = new HashSet<Device>();
            DeviceTypes = new HashSet<DeviceType>();
            OrderDetails = new HashSet<OrderDetail>();
        }

        public int DetailId { get; set; }
        public string DetailName { get; set; } = null!;
        public decimal? DetailPrice { get; set; }
        public int DetailCount { get; set; }

        public virtual ICollection<Device> Devices { get; set; }
        public virtual ICollection<DeviceType> DeviceTypes { get; set; }
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
    }
}
