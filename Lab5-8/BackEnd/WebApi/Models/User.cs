﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace WebApi.Models
{
    public partial class User: IdentityUser<int>
    {
        public string UserFirstname { get; set; } = null!;
        public string UserLastname { get; set; } = null!;

        public virtual Client? Client { get; set; }
        public virtual Employee? Employee { get; set; }
    }
}
