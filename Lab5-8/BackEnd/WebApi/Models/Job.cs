﻿using System;
using System.Collections.Generic;

namespace WebApi.Models
{
    public partial class Job
    {
        public Job()
        {
            Employees = new HashSet<Employee>();
        }

        public int JobId { get; set; }
        public string JobName { get; set; } = null!;
        public decimal JobSalary { get; set; }
        public decimal? JobPremium { get; set; }

        public virtual ICollection<Employee> Employees { get; set; }
    }
}
