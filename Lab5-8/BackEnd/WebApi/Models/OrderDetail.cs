﻿using System;
using System.Collections.Generic;

namespace WebApi.Models
{
    public partial class OrderDetail
    {
        public int DetailId { get; set; }
        public int OrderId { get; set; }
        public int DetailCount { get; set; }
        public decimal DetailsPrice { get; set; }
        public bool DetailsSpent { get; set; }

        public virtual Detail Detail { get; set; } = null!;
        public virtual Order Order { get; set; } = null!;
    }
}
