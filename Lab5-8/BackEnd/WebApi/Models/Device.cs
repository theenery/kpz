﻿using System;
using System.Collections.Generic;

namespace WebApi.Models
{
    public partial class Device
    {
        public Device()
        {
            Details = new HashSet<Detail>();
            Orders = new HashSet<Order>();
        }

        public int DeviceId { get; set; }
        public int DeviceTypeId { get; set; }
        public string DeviceMaker { get; set; } = null!;
        public string? DeviceModel { get; set; }

        public virtual DeviceType DeviceType { get; set; } = null!;
        public virtual ICollection<Detail> Details { get; set; }
        public virtual ICollection<Order> Orders { get; set; }
    }
}
