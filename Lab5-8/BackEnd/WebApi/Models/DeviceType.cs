﻿using System;
using System.Collections.Generic;

namespace WebApi.Models
{
    public partial class DeviceType
    {
        public DeviceType() { 
            Discounts = new HashSet<Discount>();
            Details = new HashSet<Detail>();
            Devices = new HashSet<Device>();
        }
        public int DeviceTypeId { get; set; }
        public string DeviceTypeName { get; set; } = null!;

        public virtual ICollection<Discount> Discounts { get; set; }
        public virtual ICollection<Detail> Details { get; set; }
        public virtual ICollection<Device> Devices { get; set; }
    }
}
