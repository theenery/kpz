﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApi.Data;
using WebApi.Models;
using WebApi.Dto;
using WebApi.Repository;
using Microsoft.AspNetCore.Authorization;

namespace WebApi.Controllers
{
    //[Authorize(Roles = "Employee")]
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class DetailController : BaseController
    {
        private readonly IDetailRepository _detailRepository;

        public DetailController(IDetailRepository detailRepository, IMapper mapper) : base(mapper)
        {
            _detailRepository = detailRepository;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<DetailDto>>> GetDetail()
        {
            return Ok(Mapper.Map<IEnumerable<DetailDto>>(await _detailRepository.GetAll().ToListAsync()));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<DetailDto>> GetDetail(int id)
        {
            var detail = await _detailRepository.GetByIdAsync(id);

            if (detail == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<DetailDto>(detail));
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutDetail(int id, DetailDto detail)
        {
            if (id != detail.DetailId)
            {
                return BadRequest();
            }

            if (!await _detailRepository.Exists(id))
            {
                return NotFound();
            }

            await _detailRepository.UpdateAsync(id, Mapper.Map<Detail>(detail));

            return NoContent();
        }

        [HttpPost]
        public async Task<ActionResult<DetailDto>> PostDetail(CreateDetailDto detail)
        {
            var detailEntity = Mapper.Map<Detail>(detail);
            await _detailRepository.CreateAsync(detailEntity);

            return CreatedAtAction("GetDetail", new { id = detailEntity.DetailId }, Mapper.Map<DetailDto>(detailEntity));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDetail(int id)
        {
            if (!await _detailRepository.Exists(id))
            {
                return NotFound();
            }

            await _detailRepository.DeleteAsync(id);

            return NoContent();
        }
    }
}