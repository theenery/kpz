﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApi.Data;
using WebApi.Models;
using WebApi.Dto;
using WebApi.Repository;
using Microsoft.AspNetCore.Authorization;

namespace WebApi.Controllers
{
    [Authorize(Roles = "Manager")]
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class ServiceController : BaseController
    {
        private readonly IServiceRepository _serviceRepository;

        public ServiceController(IServiceRepository serviceRepository, IMapper mapper) : base(mapper)
        {
            _serviceRepository = serviceRepository;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ServiceDto>>> GetService()
        {
            return Ok(Mapper.Map<IEnumerable<ServiceDto>>(await _serviceRepository.GetAll().ToListAsync()));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ServiceDto>> GetService(int id)
        {
            var service = await _serviceRepository.GetByIdAsync(id);

            if (service == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<ServiceDto>(service));
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutService(int id, ServiceDto service)
        {
            if (id != service.ServiceId)
            {
                return BadRequest();
            }

            if (!await _serviceRepository.Exists(id))
            {
                return NotFound();
            }

            await _serviceRepository.UpdateAsync(id, Mapper.Map<Service>(service));

            return NoContent();
        }

        [HttpPost]
        public async Task<ActionResult<ServiceDto>> PostService(CreateServiceDto service)
        {
            var serviceEntity = Mapper.Map<Service>(service);
            await _serviceRepository.CreateAsync(serviceEntity);

            return CreatedAtAction("GetService", new { id = serviceEntity.ServiceId }, Mapper.Map<ServiceDto>(serviceEntity));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteService(int id)
        {
            if (!await _serviceRepository.Exists(id))
            {
                return NotFound();
            }

            await _serviceRepository.DeleteAsync(id);

            return NoContent();
        }
    }
}