﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApi.Data;
using WebApi.Models;
using WebApi.Dto;
using WebApi.Repository;
using Microsoft.AspNetCore.Authorization;

namespace WebApi.Controllers
{
    [Authorize(Roles = "Client")]
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : BaseController
    {
        private readonly IOrderRepository _orderRepository;

        public OrderController(IOrderRepository orderRepository, IMapper mapper) : base(mapper)
        {
            _orderRepository = orderRepository;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<OrderDto>>> GetOrder()
        {
            return Ok(Mapper.Map<IEnumerable<OrderDto>>(await _orderRepository.GetAll().ToListAsync()));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<OrderDto>> GetOrder(int id)
        {
            var order = await _orderRepository.GetByIdAsync(id);

            if (order == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<OrderDto>(order));
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutOrder(int id, OrderDto order)
        {
            if (id != order.OrderId)
            {
                return BadRequest();
            }

            if (!await _orderRepository.Exists(id))
            {
                return NotFound();
            }

            await _orderRepository.UpdateAsync(id, Mapper.Map<Order>(order));

            return NoContent();
        }

        [HttpPost]
        public async Task<ActionResult<OrderDto>> PostOrder(CreateOrderDto order)
        {
            var orderEntity = Mapper.Map<Order>(order);
            await _orderRepository.CreateAsync(orderEntity);

            return CreatedAtAction("GetOrder", new { id = orderEntity.OrderId }, Mapper.Map<OrderDto>(orderEntity));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteOrder(int id)
        {
            if (!await _orderRepository.Exists(id))
            {
                return NotFound();
            }

            await _orderRepository.DeleteAsync(id);

            return NoContent();
        }
    }
}