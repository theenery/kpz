﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApi.Data;
using WebApi.Models;
using WebApi.Dto;
using WebApi.Repository;
using Microsoft.AspNetCore.Authorization;

namespace WebApi.Controllers
{
    //[Authorize(Roles = "Employee")]
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class OrderDetailController : BaseController
    {
        private readonly WebApiDbContext _context;

        public OrderDetailController(WebApiDbContext context, IMapper mapper) : base(mapper)
        {
            _context = context;
        }

        [Route("device-type")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DeviceType>>> GetDeviceTypes()
        {
            return Ok(_context.DeviceTypes.AsEnumerable());
        }

            // GET: api/OrderDetails/5
            [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<OrderDetail>>> GetOrderDetail(int id)
        {
            if (_context.OrderDetails == null)
            {
                return NotFound();
            }
            _context.Database.BeginTransaction();
            var orderDetail = await _context.OrderDetails.Where(d => d.OrderId == id).ToListAsync();
            _context.Database.CommitTransaction();

            if (orderDetail == null)
            {
                return NotFound();
            }

            return Ok(orderDetail);
        }

        // GET: api/OrderDetails/5
        [HttpGet("available/{id}")]
        public async Task<ActionResult<string>> GetAvailableOrderDetail(int id)
        {
            if (_context.OrderDetails == null)
            {
                return NotFound();
            }
            _context.Database.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted);
            
            var available = await _context.OrderDetails
                .Include(d => d.Detail)
                .Where(d => d.OrderId == id && !d.DetailsSpent)
                .Select(d => d.Detail.DetailCount)
                .SumAsync();

            var need = await _context.OrderDetails
                .Where(d => d.OrderId == id && !d.DetailsSpent)
                .Select(d => d.DetailCount)
                .SumAsync();

            _context.Database.CommitTransaction();

            return Ok($"{available}/{need}");
        }

        [HttpPost("available/{id}")]
        public async Task<IActionResult> SpentDetail(int id)
        {
            try
            {
                _context.Database.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted);
                _context.Database.ExecuteSqlInterpolated($"EXEC [dbo].[SpentDetails] @order_id = 1, @detail_id = {id}");
                _context.Database.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return BadRequest();
            }
        }

        // PUT: api/OrderDetails/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutOrderDetail(int id, OrderDetail orderDetail)
        {
            if (id != orderDetail.OrderId)
            {
                return BadRequest();
            }

            _context.Entry(orderDetail).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OrderDetailExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/OrderDetails
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<OrderDetail>> PostOrderDetail(OrderDetail orderDetail)
        {
            if (_context.OrderDetails == null)
            {
                return Problem("Entity set 'testContext.OrderDetail'  is null.");
            }
            _context.OrderDetails.Add(orderDetail);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (OrderDetailExists(orderDetail.OrderId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetOrderDetail", new { id = orderDetail.OrderId }, orderDetail);
        }

        // DELETE: api/OrderDetails/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteOrderDetail(int id)
        {
            if (_context.OrderDetails == null)
            {
                return NotFound();
            }
            var orderDetail = await _context.OrderDetails.FindAsync(id);
            if (orderDetail == null)
            {
                return NotFound();
            }

            _context.OrderDetails.Remove(orderDetail);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool OrderDetailExists(int id)
        {
            return (_context.OrderDetails?.Any(e => e.OrderId == id)).GetValueOrDefault();
        }
    }
}