﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApi.Data;
using WebApi.Models;

namespace WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class DeviceTypeController : BaseController
    {
        private readonly WebApiDbContext _context;

        public DeviceTypeController(WebApiDbContext context, IMapper mapper) : base(mapper)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<DeviceType>>> GetDeviceTypes()
        {
            return Ok(_context.DeviceTypes.AsEnumerable());
        }
    }
}
