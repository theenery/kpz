﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi.Services;
using WebApi.Dto;

namespace WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : BaseController
    {
        private readonly IAccountService _accountService;

        public AccountController(IAccountService accountService, IMapper mapper): base(mapper)
        {
            _accountService = accountService;
        }

        [HttpPut]
        public async Task<IActionResult> RegisterUser(RegisterUserDto user)
        {
            if (await _accountService.RegisterUser(user))
            {
                return NoContent();
            }

            return BadRequest();
        }

        [HttpPost]
        public async Task<IActionResult> LoginUser(LoginUserDto user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            if (await _accountService.LoginUser(user))
            {
                var identityUser = await _accountService.FindByEmailAsync(user.Email);
                var result = new
                {
                    Firstname = identityUser.UserFirstname,
                    Lastname = identityUser.UserLastname,
                    Role = await _accountService.GetRoleAsync(identityUser),
                    Token = await _accountService.GenerateTokenStringAsync(identityUser),
                };
                return Ok(result);
            }

            return BadRequest();
        }
    }
}
