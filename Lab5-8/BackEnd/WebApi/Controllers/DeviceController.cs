﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApi.Data;
using WebApi.Models;
using WebApi.Dto;
using WebApi.Repository;
using Microsoft.AspNetCore.Authorization;

namespace WebApi.Controllers
{
    //[Authorize(Roles = "Employee")]
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class DeviceController : BaseController
    {
        private readonly IDeviceRepository _deviceRepository;

        public DeviceController(IDeviceRepository deviceRepository, IMapper mapper) : base(mapper)
        {
            _deviceRepository = deviceRepository;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<DeviceDto>>> GetDevice()
        {
            return Ok(Mapper.Map<IEnumerable<DeviceDto>>(await _deviceRepository.GetAll().ToListAsync()));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<DeviceDto>> GetDevice(int id)
        {
            var device = await _deviceRepository.GetByIdAsync(id);

            if (device == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<DeviceDto>(device));
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutDevice(int id, DeviceDto device)
        {
            if (id != device.DeviceId)
            {
                return BadRequest();
            }

            if (!await _deviceRepository.Exists(id))
            {
                return NotFound();
            }

            await _deviceRepository.UpdateAsync(id, Mapper.Map<Device>(device));

            return NoContent();
        }

        [HttpPost]
        public async Task<ActionResult<DeviceDto>> PostDevice(CreateDeviceDto device)
        {
            var deviceEntity = Mapper.Map<Device>(device);
            await _deviceRepository.CreateAsync(deviceEntity);

            return CreatedAtAction("GetDevice", new { id = deviceEntity.DeviceId }, Mapper.Map<DeviceDto>(deviceEntity));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDevice(int id)
        {
            if (!await _deviceRepository.Exists(id))
            {
                return NotFound();
            }

            await _deviceRepository.DeleteAsync(id);

            return NoContent();
        }
    }
}