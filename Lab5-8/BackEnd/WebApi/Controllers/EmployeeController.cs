﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApi.Data;
using WebApi.Models;
using WebApi.Dto;
using WebApi.Repository;
using Microsoft.AspNetCore.Authorization;

namespace WebApi.Controllers
{
    [Authorize(Roles = "Manager")]
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : BaseController
    {
        private readonly IEmployeeRepository _employeeRepository;

        public EmployeeController(IEmployeeRepository employeeRepository, IMapper mapper) : base(mapper)
        {
            _employeeRepository = employeeRepository;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<EmployeeDto>>> GetEmployee()
        {
            return Ok(Mapper.Map<IEnumerable<EmployeeDto>>(await _employeeRepository.GetAll().ToListAsync()));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<EmployeeDto>> GetEmployee(int id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<EmployeeDto>(employee));
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutEmployee(int id, EmployeeDto employee)
        {
            if (id != employee.UserId)
            {
                return BadRequest();
            }

            if (!await _employeeRepository.Exists(id))
            {
                return NotFound();
            }

            await _employeeRepository.UpdateAsync(id, Mapper.Map<Employee>(employee));

            return NoContent();
        }

        [HttpPost]
        public async Task<ActionResult<EmployeeDto>> PostEmployee(EmployeeDto employee)
        {
            var employeeEntity = Mapper.Map<Employee>(employee);
            await _employeeRepository.CreateAsync(employeeEntity);

            return CreatedAtAction("GetEmployee", new { id = employeeEntity.UserId }, Mapper.Map<EmployeeDto>(employeeEntity));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteEmployee(int id)
        {
            if (!await _employeeRepository.Exists(id))
            {
                return NotFound();
            }

            await _employeeRepository.DeleteAsync(id);

            return NoContent();
        }
    }
}