﻿using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using WebApi.Models;
using WebApi.Dto;
using WebApi.Data;

namespace WebApi.Services
{
    public class AccountService : IAccountService
    {
        private readonly UserManager<User> _userManager;
        private readonly WebApiDbContext _context;
        private readonly IConfiguration _configuration;

        public AccountService(UserManager<User> userManager, WebApiDbContext context, IConfiguration configuration)
        {
            _userManager = userManager;
            _context = context;
            _configuration = configuration;
        }

        public async Task<User> FindByEmailAsync(string email)
        {
            return await _userManager.FindByEmailAsync(email);
        }

        public async Task<string> GenerateTokenStringAsync(User user)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Email, user.Email),
                new Claim(ClaimTypes.Role, await GetRoleAsync(user))
            };

            var securityKey = new SymmetricSecurityKey(
                Encoding.UTF8.GetBytes(_configuration.GetSection("Jwt:Key").Value));

            var signingCredentials = new SigningCredentials(
                securityKey, SecurityAlgorithms.HmacSha512Signature);

            var securityToken = new JwtSecurityToken(
                claims: claims,
                expires: DateTime.Now.AddMinutes(60),
                issuer: _configuration.GetSection("Jwt:Issuer").Value,
                audience: _configuration.GetSection("Jwt:Audience").Value,
                signingCredentials:signingCredentials);
            string tokenString = new JwtSecurityTokenHandler().WriteToken(securityToken);
            return tokenString;
        }

        public async Task<string> GetRoleAsync(User user)
        {
            var roles = await _userManager.GetRolesAsync(user);
            return roles.First();
        }

        public async Task<bool> LoginUser(LoginUserDto user)
        {
            var identityUser = await FindByEmailAsync(user.Email);
            if (identityUser == null)
            {
                return false;
            }

            return await _userManager.CheckPasswordAsync(identityUser, user.Password);
        }

        public async Task<bool> RegisterUser(RegisterUserDto user)
        {
            var identityUser = new User
            {
                Email = user.Email,
                UserFirstname = user.Firstname,
                UserLastname = user.Lastname,
                UserName = user.Firstname + user.Lastname
            };

            var creationResult = await _userManager.CreateAsync(identityUser, user.Password);
            var roleAddingResult = await _userManager.AddToRoleAsync(identityUser, "Client");
            await _context.AddAsync(new Client { UserId = identityUser.Id });
            await _context.SaveChangesAsync();
            
            return creationResult.Succeeded && roleAddingResult.Succeeded;
        }
    }
}
