﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using WebApi.Models;

namespace WebApi.Data
{
    public class WebApiDbContext : IdentityDbContext<User, Role, int>
    {
        public WebApiDbContext(DbContextOptions options) : base(options)
        {

        }

        public DbSet<Client> Clients { get; set; } = null!;
        public DbSet<Department> Departments { get; set; } = null!;
        public DbSet<Detail> Details { get; set; } = null!;
        public DbSet<Device> Devices { get; set; } = null!;
        public DbSet<DeviceType> DeviceTypes { get; set; } = null!;
        public DbSet<Discount> Discounts { get; set; } = null!;
        public DbSet<Employee> Employees { get; set; } = null!;
        public DbSet<Job> Jobs { get; set; } = null!;
        public DbSet<Order> Orders { get; set; } = null!;
        public DbSet<OrderDetail> OrderDetails { get; set; } = null!;
        public DbSet<Rewiev> Rewievs { get; set; } = null!;
        public DbSet<Service> Services { get; set; } = null!;

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Client>(entity =>
            {
                entity.HasKey(e => e.UserId);

                entity.Property(e => e.UserId)
                    .ValueGeneratedNever();

                entity.Property(e => e.ClientCity)
                    .HasMaxLength(24);

                entity.HasOne(d => d.User)
                    .WithOne(p => p.Client)
                    .HasForeignKey<Client>(d => d.UserId);
            });

            modelBuilder.Entity<Department>(entity =>
            {
                entity.Property(e => e.DepartmentId);

                entity.Property(e => e.DepartmentAdress)
                    .HasMaxLength(64);

                entity.Property(e => e.DepartmentCity)
                    .HasMaxLength(24);
            });

            modelBuilder.Entity<Detail>(entity =>
            {
                entity.Property(e => e.DetailId);

                entity.Property(e => e.DetailCount);

                entity.Property(e => e.DetailName)
                    .HasMaxLength(64);

                entity.Property(e => e.DetailPrice)
                    .HasColumnType("money");

                entity.HasMany(d => d.Devices)
                    .WithMany(p => p.Details);

                entity.HasMany(d => d.DeviceTypes)
                    .WithMany(p => p.Details);
            });

            modelBuilder.Entity<Device>(entity =>
            {
                entity.Property(e => e.DeviceId);

                entity.Property(e => e.DeviceTypeId);

                entity.Property(e => e.DeviceMaker)
                    .HasMaxLength(24);

                entity.Property(e => e.DeviceModel)
                    .HasMaxLength(24);

                entity.HasOne(d => d.DeviceType)
                    .WithMany(p => p.Devices)
                    .HasForeignKey(d => d.DeviceTypeId)
                    .OnDelete(DeleteBehavior.Cascade);

                entity.HasMany(d => d.Details)
                    .WithMany(p => p.Devices);
            });

            modelBuilder.Entity<DeviceType>(entity =>
            {
                entity.Property(e => e.DeviceTypeId);

                entity.Property(e => e.DeviceTypeName);

                entity.HasMany(d => d.Discounts)
                    .WithMany(p => p.DeviceTypes);

                entity.HasMany(d => d.Details)
                    .WithMany(p => p.DeviceTypes);
            });

            modelBuilder.Entity<Discount>(entity =>
            {
                entity.Property(e => e.DiscountId);

                entity.Property(e => e.DiscountExpiring)
                    .HasColumnType("date");

                entity.Property(e => e.DiscountStarting)
                    .HasColumnType("date");

                entity.Property(e => e.DiscountValue);

                entity.HasMany(d => d.Clients)
                    .WithMany(p => p.Discounts);

                entity.HasMany(d => d.DeviceTypes)
                    .WithMany(p => p.Discounts);
            });

            modelBuilder.Entity<Employee>(entity =>
            {
                entity.HasKey(e => e.UserId);

                entity.HasIndex(e => e.EmployeePass)
                    .IsUnique();

                entity.Property(e => e.UserId)
                    .ValueGeneratedNever();

                entity.Property(e => e.DepartmentId);

                entity.Property(e => e.EmployeePass)
                    .HasMaxLength(14)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.JobId);

                entity.HasOne(d => d.Department)
                    .WithMany(p => p.Employees)
                    .HasForeignKey(d => d.DepartmentId)
                    .OnDelete(DeleteBehavior.Cascade);

                entity.HasOne(d => d.Job)
                    .WithMany(p => p.Employees)
                    .HasForeignKey(d => d.JobId)
                    .OnDelete(DeleteBehavior.Cascade);

                entity.HasOne(d => d.User)
                    .WithOne(p => p.Employee)
                    .HasForeignKey<Employee>(d => d.UserId);
            });

            modelBuilder.Entity<Job>(entity =>
            {
                entity.Property(e => e.JobId);

                entity.Property(e => e.JobName)
                    .HasMaxLength(32);

                entity.Property(e => e.JobPremium)
                    .HasColumnType("money");

                entity.Property(e => e.JobSalary)
                    .HasColumnType("money");
            });

            modelBuilder.Entity<Order>(entity =>
            {
                entity.Property(e => e.OrderId);

                entity.Property(e => e.ClientId);

                entity.Property(e => e.EmployeeId);

                entity.Property(e => e.DeviceId);

                entity.Property(e => e.OrderDate)
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.OrderStatus)
                    .HasMaxLength(16);

                entity.Property(e => e.ServiceId);

                entity.HasOne(d => d.Client)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.ClientId)
                    .OnDelete(DeleteBehavior.Cascade);

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.EmployeeId)
                    .OnDelete(DeleteBehavior.ClientCascade);

                entity.HasOne(d => d.Device)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.DeviceId)
                    .OnDelete(DeleteBehavior.Cascade);

                entity.HasOne(d => d.Service)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.ServiceId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<OrderDetail>(entity =>
            {
                entity.HasKey(e => new { e.DetailId, e.OrderId });

                entity.Property(e => e.DetailId);

                entity.Property(e => e.OrderId);

                entity.Property(e => e.DetailCount);

                entity.Property(e => e.DetailsPrice)
                    .HasColumnType("money");

                entity.Property(e => e.DetailsSpent);

                entity.HasOne(d => d.Detail)
                    .WithMany(p => p.OrderDetails)
                    .HasForeignKey(d => d.DetailId)
                    .OnDelete(DeleteBehavior.Cascade);

                entity.HasOne(d => d.Order)
                    .WithMany(p => p.OrderDetails)
                    .HasForeignKey(d => d.OrderId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<Rewiev>(entity =>
            {
                entity.Property(e => e.RewievId);

                entity.Property(e => e.ClientId);

                entity.Property(e => e.RewievDate)
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.RewievRate);

                entity.Property(e => e.RewievText)
                    .HasColumnType("ntext");

                entity.HasOne(d => d.Client)
                    .WithMany(p => p.Rewievs)
                    .HasForeignKey(d => d.ClientId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<Service>(entity =>
            {
                entity.Property(e => e.ServiceId);

                entity.Property(e => e.ServiceGuarantee)
                    .HasMaxLength(24);

                entity.Property(e => e.ServiceName)
                    .HasMaxLength(32);

                entity.Property(e => e.ServicePrice)
                    .HasColumnType("money");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.Property(e => e.UserFirstname)
                    .HasMaxLength(24);

                entity.Property(e => e.UserLastname)
                    .HasMaxLength(24);

                entity.HasOne(d => d.Client)
                    .WithOne(p => p.User);

                entity.HasOne(d => d.Employee)
                    .WithOne(p => p.User);
            });

            base.OnModelCreating(modelBuilder);
        }
    }
}
