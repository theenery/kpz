﻿using AutoMapper;
using WebApi.Models;
using WebApi.Dto;

namespace WebApi.Data
{
    public class MappingsProfile: Profile
    {
        public MappingsProfile() {
            CreateMap<CreateServiceDto, Service>();
            CreateMap<Service, CreateServiceDto>();
            CreateMap<ServiceDto, Service>();
            CreateMap<Service, ServiceDto>();

            CreateMap<CreateDetailDto, Detail>();
            CreateMap<Detail, CreateDetailDto>();
            CreateMap<DetailDto, Detail>();
            CreateMap<Detail, DetailDto>();

            CreateMap<CreateDeviceDto, Device>();
            CreateMap<Device, CreateDeviceDto>();
            CreateMap<DeviceDto, Device>();
            CreateMap<Device, DeviceDto>();

            CreateMap<EmployeeDto, Employee>();
            CreateMap<Employee, EmployeeDto>();

            CreateMap<CreateOrderDto, Order>()
                .ForMember(dto => dto.OrderDate, opt => opt.MapFrom(src => DateTime.Now))
                .ForMember(dto => dto.EmployeeId, opt => opt.MapFrom(src => 3))
                .ForMember(dto => dto.OrderStatus, opt => opt.MapFrom(src => "New order"));
            CreateMap<Order, CreateOrderDto>();
            CreateMap<OrderDto, Order>();
            CreateMap<Order, OrderDto>();


            CreateMap<DeviceTypeDto, DeviceType>();
            CreateMap<DeviceType, DeviceTypeDto>();
            //.ForMember(dto => dto.SomeProm, opt => opt.MapFrom(src => src.OtherProp))
        }
    }
}
