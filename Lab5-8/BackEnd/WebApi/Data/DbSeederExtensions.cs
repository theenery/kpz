﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using WebApi.Models;

namespace WebApi.Data
{
    public static class DbSeederExtensions
    {
        public static IApplicationBuilder UseSqlServerSeeder(this IApplicationBuilder app)
        {
            using var scope = app.ApplicationServices.CreateScope();
            var services = scope.ServiceProvider;
            var context = services.GetRequiredService<WebApiDbContext>();
            var userManager = services.GetRequiredService<UserManager<User>>();
            var roleManager = services.GetRequiredService<RoleManager<Role>>();

            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();
            //if (context.Users.Any())
            //{
            //    return app;
            //}


            roleManager.CreateAsync(new Role { Name = "Manager" }).GetAwaiter().GetResult();
            roleManager.CreateAsync(new Role { Name = "Employee" }).GetAwaiter().GetResult();
            roleManager.CreateAsync(new Role { Name = "Client" }).GetAwaiter().GetResult();

            var user1 = new User
            {
                UserName = "Test",
                UserFirstname = "FirstTest",
                UserLastname = "LastTest",
                Email = "Test@gmail.com"
            };
            var user2 = new User
            {
                UserName = "JohnSmith",
                UserFirstname = "John",
                UserLastname = "Smith",
                Email = "JohnSmith@gmail.com"
            };
            var user3 = new User
            {
                UserName = "Test3",
                UserFirstname = "FirstTest3",
                UserLastname = "LastTest3",
                Email = "Test3@gmail.com"
            };

            userManager.CreateAsync(user1, "P@ssw0rd").GetAwaiter().GetResult();
            userManager.CreateAsync(user2, "P@ssw0rd").GetAwaiter().GetResult();
            userManager.CreateAsync(user3, "P@ssw0rd3").GetAwaiter().GetResult();

            userManager.AddToRoleAsync(user1, "Client").GetAwaiter().GetResult();
            userManager.AddToRoleAsync(user2, "Manager").GetAwaiter().GetResult();
            userManager.AddToRoleAsync(user3, "Employee").GetAwaiter().GetResult();

            context.Add(new DeviceType { DeviceTypeName = "Washing machine" });
            context.Add(new DeviceType { DeviceTypeName = "Electric stove" });
            context.Add(new DeviceType { DeviceTypeName = "Refrigerator" });

            context.Add(new Job { JobName = "Electrician", JobSalary = 15000 });
            context.Add(new Job { JobName = "Refrigerator specialist", JobSalary = 20000 });
            context.Add(new Job { JobName = "Plumber", JobSalary = 14000 });

            context.Add(new Department { DepartmentCity = "Lviv", DepartmentAdress = "Knyahuni Olgu, 59" });
            context.Add(new Department { DepartmentCity = "Lviv", DepartmentAdress = "Zelena, 32" });
            context.Add(new Department { DepartmentCity = "Ternopil", DepartmentAdress = "Zhuvova, 5" });

            context.Add(new Service { ServiceName = "Refrigerator repair", ServicePrice = 1000, ServiceGuarantee = "12m" });
            context.Add(new Service { ServiceName = "Washing machine repair", ServicePrice = 800, ServiceGuarantee = "6m" });
            context.Add(new Service { ServiceName = "Electric stove repair", ServicePrice = 600 });

            context.SaveChanges();

            context.Add(new Client { UserId = user1.Id });
            context.Add(new Employee { UserId = user3.Id, DepartmentId = 1, JobId = 1, EmployeePass = "pass" });

            context.SaveChanges();

            context.Add(new Device { DeviceTypeId = 1, DeviceMaker = "Bosch", DeviceModel = "M2" });

            context.SaveChanges();

            context.Add(new Order { ClientId = 1, EmployeeId = 3, ServiceId = 2, OrderDate = DateTime.Now, OrderStatus = "New order", DeviceId = 1 });

            context.Add(new Detail { DetailName = "Pipe 2inch", DetailPrice = 100, DetailCount = 2 });

            context.SaveChanges();

            context.Add(new OrderDetail { DetailId = 1, DetailCount = 2, DetailsPrice = 100, OrderId = 1, DetailsSpent = false });

            context.SaveChanges();

            System.FormattableString query = $"CREATE PROCEDURE SpentDetails(@order_id INT, @detail_id INT) AS BEGIN DECLARE @need_count INT, @available_count INT SET @need_count = ( SELECT od.DetailCount FROM OrderDetails AS od WHERE od.OrderId = @order_id AND od.DetailId = @detail_id) SET @available_count = ( SELECT d.DetailCount FROM Details AS d WHERE d.DetailId = @detail_id) IF @need_count IS NULL OR @available_count IS NULL BEGIN THROW 51000, 'There is no such needed or available details.', 1; END IF @need_count > @available_count BEGIN THROW 51001, 'Not enough details.', 1; END WAITFOR DELAY '00:00:05' UPDATE Details SET DetailCount = DetailCount - @need_count WHERE DetailId = @detail_id WAITFOR DELAY '00:00:05' UPDATE OrderDetails SET DetailsSpent = 1 WHERE OrderId = @order_id AND DetailId = @detail_id END";
            context.Database.ExecuteSqlInterpolated(query);

            return app;
        }
    }
}
