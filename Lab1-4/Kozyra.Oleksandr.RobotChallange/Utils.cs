﻿using System;
using System.Collections.Generic;
using System.Linq;
using Robot.Common;
using RobotInstance = Robot.Common.Robot;

namespace Kozyra.Oleksandr.RobotChallange
{
    public class Utils
    {
        public const int AttackEnergyLoss = 50;
        public const int AverageEnergyGeneration = (MaxEnergyGeneration + MinEnergyGeneration) / 2;
        public const int CreatingRobotCost = 200;
        public const int MaxEnergyCanCollect = 200;
        public const int MaxEnergyGeneration = 100;
        public const int MaxRobotCount = 100;
        public const int MaxRound = 50;
        public const int MinEnergyGeneration = 50;

        public static IRobotAlgorithm TargetAlgorithm { get; set; } = new KozyraOleksandrAlgorithm();

        public static bool CanRobotCollect(RobotInstance robot, IList<EnergyStation> stations)
        {
            return stations.Any((station) => station.Position == robot.Position);
        }

        public static bool CanRobotCreate(IList<RobotInstance> robots)
        {
            return robots.Count(IsRobotAlly) != MaxRobotCount;
        }

        public static bool IsRobotAlly(RobotInstance robot)
        {
            return robot.OwnerName == TargetAlgorithm.Author;
        }

        public static bool IsStationFree(EnergyStation station, IList<RobotInstance> robots)
        {
            return robots.All((robot) => robot.Position != station.Position);
        }

        public static int MoveCost(Position p1, Position p2)
        {
            return (p1.X - p2.X) * (p1.X - p2.X) + (p1.Y - p2.Y) * (p1.Y - p2.Y);
        }

        public static EnergyStation NearestEnemyStation(
            RobotInstance robot,
            IList<EnergyStation> stations,
            IList<RobotInstance> robots,
            out RobotInstance enemyRobot)
        {
            RobotInstance enemy = null;
            var nearestStation = stations
                .OrderBy((station) => MoveCost(robot.Position, station.Position))
                .FirstOrDefault((station) =>
                {
                    enemy = robots.SingleOrDefault((otherRobot) =>
                    {
                        return otherRobot.Position == station.Position && !IsRobotAlly(otherRobot);
                    });
                    return enemy != null;
                });
            enemyRobot = enemy;
            return nearestStation;
        }

        public static EnergyStation NearestFreeStation(
            RobotInstance robot,
            IList<EnergyStation> stations,
            IList<RobotInstance> robots)
        {
            var nearestStation = stations
                .OrderBy((station) => MoveCost(robot.Position, station.Position))
                .FirstOrDefault((station) => IsStationFree(station, robots));
            return nearestStation;
        }

        public static Position SeparateStep(Position from, Position to, int robotEnergy)
        {
            int moveCost = MoveCost(from, to) + (robotEnergy > AttackEnergyLoss ? AttackEnergyLoss : 0);
            if (moveCost < robotEnergy)
            {
                return to;
            }

            float proportion = ((float)robotEnergy / moveCost);

            return new Position(
                from.X + (int)(proportion * (to.X - from.X)),
                from.Y + (int)(proportion * (to.Y - from.Y)));
        }
    }
}
