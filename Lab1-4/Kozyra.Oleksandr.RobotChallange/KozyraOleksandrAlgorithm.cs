﻿using System;
using System.Collections.Generic;
using System.Linq;
using Robot.Common;
using RobotInstance = Robot.Common.Robot;

namespace Kozyra.Oleksandr.RobotChallange
{
    public class KozyraOleksandrAlgorithm : IRobotAlgorithm
    {
        public const int DesiredRobotMinEnergy = 100;
        private int _round = 0;

        public KozyraOleksandrAlgorithm()
        {
            Logger.OnLogRound += (sender, e) => _round = e.Number;
        }

        public string Author => "Kozyra Oleksandr";

        public RobotCommand DoStep(IList<RobotInstance> robots, int robotToMoveIndex, Map map)
        {
            var robot = robots[robotToMoveIndex];
            var stations = map.Stations;

            EnergyStation nearestFreeStation = Utils.NearestFreeStation(robot, stations, robots);
            EnergyStation nearestEnemyStation =
                Utils.NearestEnemyStation(robot, stations, robots, out var enemyOnStation);

            bool shouldCreateNewForCollect = ShouldRobotCreateNewForCollect(
                robot, nearestFreeStation, out var newRobotEnergyIfCollect);
            bool shouldCreateNewForAttack = ShouldRobotCreateNewForAttack(
                robot, enemyOnStation, nearestEnemyStation, out var newRobotEnergyIfAttack);
            bool shouldCreateNew = shouldCreateNewForCollect || shouldCreateNewForAttack;

            int lessEnergyNeed = newRobotEnergyIfCollect < newRobotEnergyIfAttack
                ? newRobotEnergyIfCollect
                : newRobotEnergyIfAttack;

            if (Utils.CanRobotCollect(robot, stations))
            {
                if (shouldCreateNew && Utils.CanRobotCreate(robots))
                {
                    return new CreateNewRobotCommand() { NewRobotEnergy = lessEnergyNeed };
                }
                return new CollectEnergyCommand();
            }

            // as i calculate profitable strategy for creating new robot using current robot
            // i can use results for moving current

            var targetStation = newRobotEnergyIfCollect < newRobotEnergyIfAttack
                ? nearestFreeStation
                : nearestEnemyStation;
            if (targetStation == null)
            {
                return new MoveCommand() { NewPosition = robot.Position };
            }
            var targetPosition = Utils.SeparateStep(robot.Position, targetStation.Position, robot.Energy);
            return new MoveCommand() { NewPosition = targetPosition };
        }

        public int MinEnergyCanCollect(EnergyStation fromStation, int perRounds)
        {
            int allEnergy = fromStation.Energy + Utils.MinEnergyGeneration * perRounds;
            int roundsNeedToCollectAll = allEnergy / Utils.MaxEnergyCanCollect;

            if (roundsNeedToCollectAll > perRounds)
            {
                return Utils.MaxEnergyCanCollect * perRounds;
            }

            return allEnergy;
        }

        public bool ShouldRobotCreateNewForCollect(
            RobotInstance robot, 
            EnergyStation targetStation,
            out int newRobotEnergy)
        {
            int roundsLeft = Utils.MaxRound - _round;

            if (targetStation == null)
            {
                newRobotEnergy = int.MaxValue;
                return false;
            }
            int moveCost = Utils.MoveCost(robot.Position, targetStation.Position);
            newRobotEnergy = moveCost + DesiredRobotMinEnergy;
            int energyLoss = moveCost + Utils.CreatingRobotCost;

            int notCreateEnergyProfit = roundsLeft * Utils.MinEnergyGeneration;
            int currentRobotCanCollect = (roundsLeft - 1) * Utils.MinEnergyGeneration;
            int createdRobotCanCollect = MinEnergyCanCollect(targetStation, roundsLeft - 1);
            int ifCreateEnergyProfit = currentRobotCanCollect + createdRobotCanCollect - energyLoss;

            bool shouldCreate = notCreateEnergyProfit < ifCreateEnergyProfit;
            bool canCreate = robot.Energy - DesiredRobotMinEnergy > energyLoss + DesiredRobotMinEnergy;
            return shouldCreate && canCreate;
        }

        public bool ShouldRobotCreateNewForAttack(
            RobotInstance robot,
            RobotInstance enemy,
            EnergyStation targetStation,
            out int newRobotEnergy)
        {
            int roundsLeft = Utils.MaxRound - _round;

            if (targetStation == null)
            {
                newRobotEnergy = int.MaxValue;
                return false;
            }
            int moveCost = Utils.MoveCost(robot.Position, targetStation.Position);
            newRobotEnergy = moveCost + DesiredRobotMinEnergy + enemy.Energy;
            int maxEnergyLoss = moveCost + Utils.CreatingRobotCost + enemy.Energy;

            int notCreateEnergyProfit = roundsLeft * Utils.MinEnergyGeneration;
            int currentRobotCanCollect = (roundsLeft - 1) * Utils.MinEnergyGeneration;
            int maxFightRounds = enemy.Energy / Utils.AttackEnergyLoss;
            int createdRobotCanCollect = (roundsLeft - 1 - maxFightRounds) * Utils.MinEnergyGeneration;
            int ifCreateEnergyProfit = currentRobotCanCollect + createdRobotCanCollect - maxEnergyLoss;

            bool shouldCreate = notCreateEnergyProfit < ifCreateEnergyProfit;
            bool canCreate = robot.Energy - DesiredRobotMinEnergy > maxEnergyLoss + DesiredRobotMinEnergy;
            return shouldCreate && canCreate;
        }
    }
}
