using System;
using System.Collections.Generic;
using Xunit;
using Robot.Common;
using RobotInstance = Robot.Common.Robot;

namespace Kozyra.Oleksandr.RobotChallange.Test
{
    public class TestAlgorithm
    {
        private KozyraOleksandrAlgorithm _algorithm = new KozyraOleksandrAlgorithm();
        private List<RobotInstance> _robots = new List<RobotInstance>();
        private List<EnergyStation> _stations = new List<EnergyStation>();
        private Map _map = new Map()
        {
            MaxPozition = new Position(100, 100),
            MinPozition = new Position(0, 0),
        };

        public TestAlgorithm()
        {
            _map.Stations = _stations;
        }

        [Fact]
        public void TestDoStepCreateForCollect()
        {

            int moveCost = 8;
            int minEnergyToCreate = Utils.CreatingRobotCost +
                                    KozyraOleksandrAlgorithm.DesiredRobotMinEnergy * 2 +
                                    moveCost + 1;

            _robots.Add(new RobotInstance() { Position = new Position(1, 1), Energy = minEnergyToCreate });
            _stations.Add(new EnergyStation() { Position = new Position(1, 1) });
            _stations.Add(new EnergyStation() { Position = new Position(3, 3) });
            var expectedEnergy = KozyraOleksandrAlgorithm.DesiredRobotMinEnergy + moveCost;

            var command = _algorithm.DoStep(_robots, 0, _map);

            Assert.True(command is CreateNewRobotCommand);
            Assert.Equal(expectedEnergy, (command as CreateNewRobotCommand).NewRobotEnergy);
        }

        [Fact]
        public void TestDoStepCreateForAttack()
        {
            int moveCost = 8;
            int enemyEnergy = 5;
            int minEnergyToCreate = Utils.CreatingRobotCost +
                                    KozyraOleksandrAlgorithm.DesiredRobotMinEnergy * 2 +
                                    moveCost + 1 +
                                    enemyEnergy;

            _robots.Add(new RobotInstance()
            {
                Position = new Position(1, 1),
                Energy = minEnergyToCreate,
                OwnerName = _algorithm.Author
            });
            _robots.Add(new RobotInstance()
            {
                Position = new Position(3, 3),
                Energy = enemyEnergy,
                OwnerName = "f"
            });
            _stations.Add(new EnergyStation() { Position = new Position(1, 1) });
            _stations.Add(new EnergyStation() { Position = new Position(3, 3) });
            var expectedEnergy = KozyraOleksandrAlgorithm.DesiredRobotMinEnergy + moveCost + enemyEnergy;

            var command = _algorithm.DoStep(_robots, 0, _map);

            Assert.True(command is CreateNewRobotCommand);
            Assert.Equal(expectedEnergy, (command as CreateNewRobotCommand).NewRobotEnergy);
        }

        [Fact]
        public void TestDoStepCollect()
        {
            var position = new Position(1, 1);
            _robots.Add(new RobotInstance() { Position = position, Energy = 100 });
            _stations.Add(new EnergyStation() { Position = position });

            var command = _algorithm.DoStep(_robots, 0, _map);

            Assert.True(command is CollectEnergyCommand);
        }

        [Fact]
        public void TestDoStepMove()
        {
            _robots.Add(new RobotInstance() { Position = new Position(1, 1), Energy = 100 });
            _stations.Add(new EnergyStation() { Position = new Position(3, 3) });
            var expectedNewPosition = _stations[0].Position;

            var command = _algorithm.DoStep(_robots, 0, _map);

            Assert.True(command is MoveCommand);
            Assert.Equal(expectedNewPosition, (command as MoveCommand).NewPosition);
        }

        [Fact]
        public void TestMinEnergyCanCollect()
        {
            var algorithm = new KozyraOleksandrAlgorithm();
            var station = new EnergyStation() { Energy = 600 };
            var rounds = 4;
            var expectedResult = 600 + Utils.MinEnergyGeneration * rounds;

            var result = algorithm.MinEnergyCanCollect(station, rounds);

            Assert.Equal(expectedResult, result);

            rounds = 2;
            expectedResult = 400;

            result = algorithm.MinEnergyCanCollect(station, rounds);

            Assert.Equal(expectedResult, result);
        }
    }
}
