﻿using System;
using System.Collections.Generic;
using Xunit;
using Robot.Common;
using RobotInstance = Robot.Common.Robot;

namespace Kozyra.Oleksandr.RobotChallange.Test
{
    public class TestUtils
    {
        [Fact]
        public void TestCanRobotCollect()
        {
            var robot = new RobotInstance() { Position = new Position(1, 1) };
            var stations = new List<EnergyStation>()
            {
                new EnergyStation() { Position = new Position(2, 2) },
                new EnergyStation() { Position = new Position(3, 3) }
            };

            Assert.False(Utils.CanRobotCollect(robot, stations));

            stations.Add(new EnergyStation() { Position = robot.Position });

            Assert.True(Utils.CanRobotCollect(robot, stations));
        }

        [Fact]
        public void TestIsRobotAlly()
        {
            var algorithm = new KozyraOleksandrAlgorithm();
            var robot = new RobotInstance() { OwnerName = algorithm.Author };

            Assert.True(Utils.IsRobotAlly(robot));

            robot.OwnerName = "hello";

            Assert.False(Utils.IsRobotAlly(robot));
        }

        [Fact]
        public void TestIsStationFree()
        {
            var station = new EnergyStation() { Position = new Position(1, 1) };
            var robots = new List<RobotInstance>
            {
                new RobotInstance() { Position = new Position(2, 2) },
                new RobotInstance() { Position = new Position(3, 3) }
            };

            Assert.True(Utils.IsStationFree(station, robots));

            robots.Add(new RobotInstance() { Position = station.Position });

            Assert.False(Utils.IsStationFree(station, robots));
        }

        [Fact]
        public void TestMoveCost()
        {
            var p1 = new Position(10, 5);
            var p2 = new Position(7, 13);

            Assert.Equal(73, Utils.MoveCost(p1, p2));
        }

        [Fact]
        public void TestNearestEnemyStation()
        {
            var stations = new List<EnergyStation>()
            {
                new EnergyStation() { Position = new Position(2, 2) }, // free
                new EnergyStation() { Position = new Position(3, 3) }, // not closer
                new EnergyStation() { Position = new Position(3, 2) }, // free
                new EnergyStation() { Position = new Position(6, 9) }, // <<< closer enemy
            };
            var robots = new List<RobotInstance>()
            {
                new RobotInstance() { Position = new Position(6, 7) }, // <<< current robot
                new RobotInstance() { Position = new Position(3, 3) },
                new RobotInstance() { Position = new Position(6, 9) }, // <<< enemy robot
            };

            Assert.Equal(stations[3], Utils.NearestEnemyStation(robots[0], stations, robots, out var enemy));
            Assert.Equal(robots[2], enemy);
        }

        [Fact]
        public void TestNearestFreeStation()
        {
            var stations = new List<EnergyStation>()
            {
                new EnergyStation() { Position = new Position(2, 2) }, // free but not closer
                new EnergyStation() { Position = new Position(3, 3) }, // closer but not free
                new EnergyStation() { Position = new Position(3, 2) }, // <<<
                new EnergyStation() { Position = new Position(6, 9) }, // closer but not free
            };
            var robots = new List<RobotInstance>()
            {
                new RobotInstance() { Position = new Position(6, 7) }, // <<<
                new RobotInstance() { Position = new Position(3, 3) },
                new RobotInstance() { Position = new Position(6, 9) },
            };

            Assert.Equal(stations[2], Utils.NearestFreeStation(robots[0], stations, robots));
        }

        [Theory]
        [InlineData(5, 2, 4)]
        [InlineData(5, 3, 8)]
        [InlineData(5, 5, 30)]
        [InlineData(5, 5, 100)]
        [InlineData(21, 5, 100)]
        public void TestSeparateStep(int stationPositionY, int expectedPositionY, int robotEnergy)
        {
            Position robotPosition = new Position(1, 1);
            Position stationPosition = new Position(1, stationPositionY);
            Position expectedTargetPosition = new Position(1, expectedPositionY);

            Position targetPosition = Utils.SeparateStep(robotPosition, stationPosition, robotEnergy);

            Assert.Equal(expectedTargetPosition, targetPosition);
        }
    }
}
